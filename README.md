<img src="./frontend/src/assets/screenshots/project-logo.png" alt="logo" width="250px"/>

<br>
<br>

**Final Project Assignment for 'Telerik Academy Alpha' with JavaScript**

A full stack web application that assists with project management. The system is only accessible by manager level users.
Managers can view the full list of employees/projects and their details. They can create and manage their own projects.
Admin managers have some extra capabilities like adding new employees and skills to the system.

**Original project location:**
https://gitlab.com/ghost_fx1337/project-planning-system

**Deployed project:**
https://project-flow42.herokuapp.com

- admin email: **admin@domain.com**
- admin password: **!1Password**
- manager email: **manager1@domain.com**
- manager password: **!1Password**

(See the known bugs section below.)

#### Authors:

- Kristiyan Kolev
- Georgi Tsvetkov

#### Front-end:

- Angular 9

### Screenshots:

<hr>
<img src="./frontend/src/assets/screenshots/homepage.png" alt="logo" width="250px"/>
<img src="./frontend/src/assets/screenshots/employees-page.png" alt="logo" width="250px"/>
<img src="./frontend/src/assets/screenshots/project-details.png" alt="logo" width="250px"/>
<hr>

#### Back-end:

- NestJS
- MySQL

### DB Schema:

<hr>
<img src="./frontend/src/assets/screenshots/DB.png" alt="logo" width="300px"/>
<hr>

## Getting started

### Installation

Run git clone https://gitlab.com/KristiyanKolev/project-planning-system.git

### Setup environment

**1** The back-end needs a **.env** file and an **ormconfig.json** file.

- .env should look like this:

```
PORT = 3000
DB_TYPE = mysql
DB_HOST = localhost
DB_PORT = 3306
DB_USERNAME = undefined // Replace with your database username.
DB_PASSWORD = undefined // Replace with your databese password.
DB_DATABASE_NAME = undefined // Replace with your database name.
JWT_SECRET = yourSecret
```

- ormconfig.json should look like this:

```json
{
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "undefined", // Replace with your database username.
  "password": "undefined", // Replace with your databese password.
  "database": "undefined", // Replace with your database name.
  "entities": ["src/data/entities/**/*.ts"],
  "migrations": ["src/data/migration/**/*.ts"],
  "cli": {
    "entitiesDir": "src/data/entities",
    "migrationsDir": "src/data/migration"
  }
}
```

**2.** Run **npm install** in both the back-end and front-end folders to install all dependencies

**3.** Run **npm run start:dev** to start the server

**4.** Run **npm run seed** to seed some initial data

**5.** Run **ng serve** to start the client

**6.** Go to **http://localhost:4200/**

**KNOWN BUGS**

- You should always have a management activity when creating a project and there is nothing which indicates this.

- Managers with admin role tamper heavily with the projects logic, currently only managers who are not admins will create projects which will be properly displayed with all necessary details.

- When editing a project you need to click each user so that their hours are re-calculated, otherwise you can add above allowed hours input.

- No validation for input type or negative number values when choosing project activity and member hours and target days to complete.

- While creating a new project you can add the same activity multiple times.

- While creating a new project you can assign the same member to an activity multiple times.

- While creating a new employee you can assign it the same skill multiple times, though that does not create an employee with duplicate skills.
