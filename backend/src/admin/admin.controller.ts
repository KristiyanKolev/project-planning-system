import { UserDTO } from './../models/user/user.dto';
import { Controller, UseGuards, Get, Post, Body, Patch, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { APIResponse } from '../models/user/api-response.dto';
import { AdminService } from './admin.service';
import { SkillDTO } from '../models/admin/skill.dto';
import { Skill } from '../data/entities/skill.entity';
import { CreateUserDTO } from '../models/user/create-user.dto';

@Controller()
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('admin/skills')
  async createSkill(@Body() skillDTO: SkillDTO): Promise<APIResponse<Skill>> {
    return this.serializeResponse(await this.adminService.createSkill(skillDTO));
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/skills')
  async getAllSkills(): Promise<APIResponse<SkillDTO[]>> {
    return this.serializeResponse(await this.adminService.getAllSkills());
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('admin/users')
  async createEmployee(@Body() userDTO: CreateUserDTO): Promise<APIResponse<UserDTO>> {
    return this.serializeResponse(await this.adminService.createUser(userDTO));
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch('admin/users/:id')
  async updateUser(
    @Param('id') id: string,
    @Body() userDTO: UserDTO,
  ): Promise<APIResponse<UserDTO>> {
    return this.serializeResponse(await this.adminService.updateUser(id, userDTO));
  }

  private serializeResponse<T>(data: T): APIResponse<T> {
    return { data };
  }
}
