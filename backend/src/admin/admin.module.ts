import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Skill } from '../data/entities/skill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Skill])],
  controllers: [AdminController],
  providers: [AdminService],
})
export class AdminModule {}
