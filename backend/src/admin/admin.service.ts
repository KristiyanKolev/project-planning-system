import { SkillDTO } from './../models/admin/skill.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Repository } from 'typeorm';
import { UserDTO } from '../models/user/user.dto';
import { plainToClass } from 'class-transformer';
import { Skill } from '../data/entities/skill.entity';
import { CreateUserDTO } from '../models/user/create-user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(User) private readonly usersRepo: Repository<User>,
    @InjectRepository(Skill) private readonly skillsRepo: Repository<Skill>,
  ) {}

  async createUser(userDTO: CreateUserDTO): Promise<UserDTO> {
    let newUser = new User();
    await this.populateUser(newUser, userDTO);
    const savedUser = await this.usersRepo.save(newUser);
    const converted = plainToClass(UserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
    return converted;
  }

  async updateUser(id, userDTO): Promise<UserDTO> {
    if (+id !== userDTO.id) {
      return null;
    }
    let user = await this.usersRepo.findOne({
      where: { id },
    });
    await this.populateUser(user, userDTO);
    const savedUser = await this.usersRepo.save(user);
    const converted = plainToClass(UserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
    return converted;
  }

  async createSkill(skillDTO: SkillDTO): Promise<Skill> {
    const skill = new Skill();
    skill.skill = skillDTO.skill;
    return await this.skillsRepo.save(skill);
  }

  async getAllSkills(): Promise<SkillDTO[]> {
    let skills = await this.skillsRepo.find();
    const converted = plainToClass(SkillDTO, skills, {
      excludeExtraneousValues: true,
    });
    return converted;
  }

  async populateUser(user, userDTO) {
    user.firstName = userDTO.firstName;
    user.surname = userDTO.surname;
    user.email = userDTO.email;
    user.position = userDTO.position;
    user.role = userDTO.role;

    if (userDTO.password) {
      user.password = await bcrypt.hash(userDTO.password, 10);
    }
    // Get all user skills from DB and set the relation
    if (userDTO.skills) {
      let newUserSkills = userDTO.skills.map(el => {
        return { id: el.id };
      });
      let skills = await this.skillsRepo.find({
        where: newUserSkills,
      });
      user.skills = Promise.resolve(skills);
    } else {
      user.skills = [];
    }

    // Get manager from DB and set managedBy relation
    if (userDTO.managedBy) {
      let managedBy = await this.usersRepo.findOne({
        where: { id: userDTO.managedBy.id },
      });
      user.managedBy = Promise.resolve(managedBy);
    } else {
      user.managedBy = undefined;
    }
  }
}
