import { LoginCredentials } from './../models/auth/login-credentials.dto';
import { AuthService } from './auth.service';
import { Controller, Post, Body } from '@nestjs/common';
import { TokenDTO } from '../models/auth/token.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(@Body() credentials: LoginCredentials): Promise<TokenDTO> {
    return this.authService.login(credentials);
  }
}
