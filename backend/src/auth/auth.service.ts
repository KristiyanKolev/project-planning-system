import { CreateUserDTO } from './../models/user/create-user.dto';
import { LoginCredentials } from './../models/auth/login-credentials.dto';
import { Injectable, BadRequestException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserDTO } from '../models/user/user.dto';
import * as bcrypt from 'bcrypt';
import { User } from '../data/entities/user.entity';
import { TokenDTO } from '../models/auth/token.dto';
import { TokenPayload } from '../models/auth/token-payload.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly usersRepo: Repository<User>,
    private readonly jwt: JwtService,
  ) {}

  async validateUser(email: string): Promise<UserDTO> {
    const user = await this.usersRepo.findOne({
      where: { email },
    });

    if (user) {
      let resultUser = new UserDTO();
      resultUser.id = user.id;
      return resultUser;
    }

    return null;
  }

  async login(credentials: LoginCredentials): Promise<TokenDTO> {
    const foundUser: User = await this.usersRepo.findOne({
      where: { email: credentials.email },
    });
    if (!foundUser || !(await bcrypt.compare(credentials.password, foundUser.password))) {
      throw new UnauthorizedException();
    }
    const payload: TokenPayload = {
      id: foundUser.id,
      firstName: foundUser.firstName,
      surname: foundUser.surname,
      email: foundUser.email,
      role: foundUser.role,
      position: foundUser.position,
    };

    return this.signToken(payload);
  }

  async register(createUser: CreateUserDTO): Promise<Object> {
    const foundUser = await this.usersRepo.findOne({
      where: { email: createUser.email },
    });

    if (foundUser) {
      throw new BadRequestException('A user with this email already exists in the system!');
    }

    createUser.password = await bcrypt.hash(createUser.password, 10);
    const user = new User();
    user.firstName = createUser.firstName;
    user.surname = createUser.surname;
    user.password = createUser.password;
    user.email = createUser.email;
    user.role = createUser.role;
    user.position = createUser.position;

    const createdUser = await this.usersRepo.save(user);
    const payload = {
      id: createdUser.id,
      email: createdUser.email,
    };
    return { data: 'Registration successful!' };
  }

  private async signToken(payload: TokenPayload): Promise<TokenDTO> {
    return {
      token: this.jwt.sign(payload),
    };
  }
}
