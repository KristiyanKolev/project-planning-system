import { Activity } from './activity.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.entity';

@Entity('members')
export class Member {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  hours: number;

  @Column()
  startDate: string;

  @Column({ nullable: true })
  endDate: string;

  @ManyToOne(
    type => User,
    user => user.projectsMember,
  )
  user: Promise<User>;

  @ManyToOne(
    type => Activity,
    activity => activity.members,
  )
  activity: Promise<Activity>;
}
