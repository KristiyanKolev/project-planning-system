import { Activity } from './activity.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ProjectStatus } from '../../common/enums/project-status.enum';

@Entity('projects')
export class Project {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('nvarchar')
  name: string;

  @Column('nvarchar')
  description: string;

  @Column()
  targetInDays: number;

  @Column()
  startDate: string;

  @Column({ type: 'enum', enum: ProjectStatus, default: ProjectStatus.inProgress })
  status: ProjectStatus;

  @OneToMany(
    type => Activity,
    activity => activity.project,
  )
  activities: Promise<Activity[]>;
}
