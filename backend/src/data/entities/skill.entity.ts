import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Activity } from './activity.entity';

@Entity('skills')
export class Skill {
  @PrimaryGeneratedColumn('increment')
  id: string;

  @Column('nvarchar')
  skill: string;

  @OneToMany(
    type => Activity,
    activity => activity.skill,
  )
  activities: Promise<Activity[]>;
}
