import { Repository } from 'typeorm';
import { UserRoles } from '../../common/enums/user-role.enum';
import { User } from '../entities/user.entity';
import { Skill } from '../entities/skill.entity';
import * as bcrypt from 'bcrypt';

export const seedAdmin = async (connection: any): Promise<void> => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const skillRepo: Repository<Skill> = connection.manager.getRepository(Skill);

  const management: Skill = await skillRepo.findOne({
    where: { skill: 'management' },
  });

  if (!management) {
    console.log('Management skill not found in the DB!');
    return;
  }

  const admin: User = await userRepo.findOne({
    where: {
      role: UserRoles.admin,
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const skills: Skill[] = [management];

  const newAdmin: User = userRepo.create({
    firstName: 'Blagoi',
    surname: 'Todorov',
    position: 'Admin',
    email: 'admin@domain.com',
    password: hashedPassword,
    role: UserRoles.admin,
  });

  newAdmin.skills = Promise.resolve(skills);
  await userRepo.save(newAdmin);

  console.log('Seeded admin successfully!');
};
