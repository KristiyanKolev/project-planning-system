import { Repository } from 'typeorm';
import { UserRoles } from '../../common/enums/user-role.enum';
import { User } from '../entities/user.entity';
import { Skill } from '../entities/skill.entity';
import * as bcrypt from 'bcrypt';

export const seedEmployees = async (connection: any): Promise<void> => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const skillRepo: Repository<Skill> = connection.manager.getRepository(Skill);

  const skillsList: Skill[] = await skillRepo.find();

  if (!skillsList) {
    console.log('Skills not found in the DB!');
    return;
  }

  const admin: User = await userRepo.findOne({
    where: {
      role: UserRoles.admin,
    },
  });

  if (!admin) {
    console.log('Admin not found in the DB!');
    return;
  }

  const managers: User[] = await userRepo.find({
    where: {
      role: UserRoles.manager,
    },
  });

  if (!managers) {
    console.log('Managers not found in the DB!');
    return;
  }

  const employee: User = await userRepo.findOne({
    where: {
      role: UserRoles.basic,
    },
  });

  if (employee) {
    console.log('The DB already has employees!');
    return;
  }

  const javaScript: Skill = skillsList[0];
  const typeScript: Skill = skillsList[1];
  const python: Skill = skillsList[2];
  const java: Skill = skillsList[3];
  const c: Skill = skillsList[4];
  const cPlusPlus: Skill = skillsList[5];
  const cSharp: Skill = skillsList[6];
  const angular: Skill = skillsList[7];
  const react: Skill = skillsList[8];
  const management: Skill = skillsList[9];

  const managerOne = managers[0];
  const managerTwo = managers[1];

  const E1hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const E1skills: Skill[] = [javaScript, typeScript, angular, react];

  const employeeOne: User = userRepo.create({
    firstName: 'Iliana',
    surname: 'Dobreva',
    position: 'Developer',
    email: 'employee1@domain.com',
    password: E1hashedPassword,
    role: UserRoles.basic,
  });

  employeeOne.skills = Promise.resolve(E1skills);
  employeeOne.managedBy = Promise.resolve(managerOne);
  await userRepo.save(employeeOne);

  const E2hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const E2skills: Skill[] = [cSharp, python, java, javaScript];

  const employeeTwo: User = userRepo.create({
    firstName: 'Tihomir',
    surname: 'Vasilov',
    position: 'Developer',
    email: 'employee2@domain.com',
    password: E2hashedPassword,
    role: UserRoles.basic,
  });

  employeeTwo.skills = Promise.resolve(E2skills);
  employeeTwo.managedBy = Promise.resolve(managerOne);
  await userRepo.save(employeeTwo);

  const E3hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const E3skills: Skill[] = [c, cPlusPlus, cSharp, python, java];

  const employeeThree: User = userRepo.create({
    firstName: 'Stanislava',
    surname: 'Kirilova',
    position: 'Developer',
    email: 'employee3@domain.com',
    password: E3hashedPassword,
    role: UserRoles.basic,
  });

  employeeThree.skills = Promise.resolve(E3skills);
  employeeThree.managedBy = Promise.resolve(managerTwo);
  await userRepo.save(employeeThree);

  const E4hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const E4skills: Skill[] = [javaScript];

  const employeeFour: User = userRepo.create({
    firstName: 'Kaloyan',
    surname: 'Mladenov',
    position: 'Intern',
    email: 'employee4@domain.com',
    password: E4hashedPassword,
    role: UserRoles.basic,
  });

  employeeFour.skills = Promise.resolve(E4skills);
  employeeFour.managedBy = Promise.resolve(managerTwo);
  await userRepo.save(employeeFour);

  managerOne.subordinates = Promise.resolve([employeeOne, employeeTwo]);
  managerTwo.subordinates = Promise.resolve([employeeThree, employeeFour]);
  await userRepo.save(managerOne);
  await userRepo.save(managerTwo);

  console.log('Seeded employees successfully!');
};
