import { Repository } from 'typeorm';
import { UserRoles } from '../../common/enums/user-role.enum';
import { User } from '../entities/user.entity';
import { Skill } from '../entities/skill.entity';
import * as bcrypt from 'bcrypt';

export const seedManagers = async (connection: any): Promise<void> => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const skillRepo: Repository<Skill> = connection.manager.getRepository(Skill);

  const management: Skill = await skillRepo.findOne({
    where: { skill: 'management' },
  });

  if (!management) {
    console.log('Management skill not found in the DB!');
    return;
  }

  const admin: User = await userRepo.findOne({
    where: {
      role: UserRoles.admin,
    },
  });

  if (!admin) {
    console.log('Admin not found in the DB!');
    return;
  }

  const manager: User = await userRepo.findOne({
    where: {
      role: UserRoles.manager,
    },
  });

  if (manager) {
    console.log('The DB already has managers!');
    return;
  }

  const M1hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const M1skills: Skill[] = [management];

  const managerOne: User = userRepo.create({
    firstName: 'Vladimir',
    surname: 'Manolev',
    position: 'Manager',
    email: 'manager1@domain.com',
    password: M1hashedPassword,
    role: UserRoles.manager,
  });

  managerOne.skills = Promise.resolve(M1skills);
  managerOne.managedBy = Promise.resolve(admin);
  await userRepo.save(managerOne);

  const M2hashedPassword: string = await bcrypt.hash('!1Password', 10);
  const M2skills: Skill[] = [management];

  const managerTwo: User = userRepo.create({
    firstName: 'Tsvetanka',
    surname: 'Ganeva',
    position: 'Manager',
    email: 'manager2@domain.com',
    password: M2hashedPassword,
    role: UserRoles.manager,
  });

  managerTwo.skills = Promise.resolve(M2skills);
  managerTwo.managedBy = Promise.resolve(admin);
  await userRepo.save(managerTwo);

  admin.subordinates = Promise.resolve([managerOne, managerTwo]);
  await userRepo.save(admin);

  console.log('Seeded managers successfully!');
};
