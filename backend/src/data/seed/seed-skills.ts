import { Repository } from 'typeorm';
import { Skill } from '../entities/skill.entity';

export const seedSkills = async (connection: any): Promise<void> => {
  const skillRepo: Repository<Skill> = connection.manager.getRepository(Skill);

  const skill: Skill = await skillRepo.findOne();

  if (skill) {
    console.log('The DB already has skills!');
    return;
  }

  const javaScript: Skill = skillRepo.create({ skill: 'javascript' });
  await skillRepo.save(javaScript);

  const typeScript: Skill = skillRepo.create({ skill: 'typescript' });
  await skillRepo.save(typeScript);

  const python: Skill = skillRepo.create({ skill: 'python' });
  await skillRepo.save(python);

  const java: Skill = skillRepo.create({ skill: 'java' });
  await skillRepo.save(java);

  const c: Skill = skillRepo.create({ skill: 'c' });
  await skillRepo.save(c);

  const cPlusPlus: Skill = skillRepo.create({ skill: 'c++' });
  await skillRepo.save(cPlusPlus);

  const cSharp: Skill = skillRepo.create({ skill: 'c#' });
  await skillRepo.save(cSharp);

  const angular: Skill = skillRepo.create({ skill: 'angular' });
  await skillRepo.save(angular);

  const react: Skill = skillRepo.create({ skill: 'react' });
  await skillRepo.save(react);

  const management: Skill = skillRepo.create({ skill: 'management' });
  await skillRepo.save(management);

  console.log('Seeded skills successfully!');
};
