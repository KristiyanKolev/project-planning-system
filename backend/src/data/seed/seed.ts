import { createConnection } from 'typeorm';
import { seedSkills } from './seed-skills';
import { seedAdmin } from './seed-admin';
import { seedManagers } from './seed-managers';
import { seedEmployees } from './seed-employees';

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedSkills(connection);
  await seedAdmin(connection);
  await seedManagers(connection);
  await seedEmployees(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
