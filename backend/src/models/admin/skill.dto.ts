import { Expose } from 'class-transformer';

export class SkillDTO {
  @Expose()
  id: string;

  @Expose()
  skill: string;
}
