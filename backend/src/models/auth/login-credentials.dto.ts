import { IsNotEmpty, Length } from 'class-validator';

export class LoginCredentials {
  @IsNotEmpty()
  @Length(5, 50)
  email: string;

  @IsNotEmpty()
  password: string;
}
