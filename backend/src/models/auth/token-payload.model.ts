import { UserRoles } from '../../common/enums/user-role.enum';

export class TokenPayload {
  id: string;
  firstName: string;
  surname: string;
  email: string;
  role: UserRoles;
  position: string;
}
