import { ActivityDTO } from '../../models/project/activity.dto';
import { UserDTO } from '../../models/user/user.dto';
import { IsNotEmpty } from 'class-validator';
import { Expose } from 'class-transformer';

export class MemberDTO {
  @Expose()
  id: string;

  @Expose()
  @IsNotEmpty()
  hours: number;

  @Expose()
  startDate: string;

  @Expose()
  endDate: string;

  user: UserDTO;

  activity: ActivityDTO;
}
