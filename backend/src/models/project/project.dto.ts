import { ActivityDTO } from './activity.dto';
import { IsNotEmpty, Length } from 'class-validator';
import { Expose } from 'class-transformer';
import { ProjectStatus } from '../../common/enums/project-status.enum';

export class ProjectDTO {
  @Expose()
  id: string;

  @Expose()
  @IsNotEmpty()
  @Length(2, 20)
  name: string;

  @Expose()
  @IsNotEmpty()
  description: string;

  @Expose()
  @IsNotEmpty()
  targetInDays: number;

  @Expose()
  status: ProjectStatus;

  @Expose()
  startDate: string;

  @IsNotEmpty()
  activities: ActivityDTO[];
}
