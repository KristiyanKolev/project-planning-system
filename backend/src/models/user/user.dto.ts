import { MemberDTO } from './../project/member.dto';
import { SkillDTO } from './../admin/skill.dto';
import { Expose } from 'class-transformer';
import { UserRoles } from '../../common/enums/user-role.enum';

export class UserDTO {
  @Expose()
  id: string;

  @Expose()
  firstName: string;

  @Expose()
  surname: string;

  @Expose()
  email: string;

  @Expose()
  position: string;

  @Expose()
  role: UserRoles;

  managedBy: UserDTO;

  skills: SkillDTO[];

  subordinates: UserDTO[];

  projectsMember: MemberDTO[];
}
