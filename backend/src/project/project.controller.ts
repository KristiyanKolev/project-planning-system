import { Controller, UseGuards, Post, Body, Get, Param, Patch } from '@nestjs/common';
import { ProjectService } from './project.service';
import { AuthGuard } from '@nestjs/passport';
import { APIResponse } from '../models/user/api-response.dto';
import { ProjectDTO } from '../models/project/project.dto';
import { ProjectStatus } from 'src/common/enums/project-status.enum';

@Controller('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createProject(@Body() projectDTO: ProjectDTO): Promise<APIResponse<ProjectDTO>> {
    return this.serializeResponse(await this.projectService.createProject(projectDTO));
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getProject(@Param('id') id: string): Promise<APIResponse<ProjectDTO>> {
    return this.serializeResponse(await this.projectService.getProject(id));
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch(':id')
  async updateProject(
    @Param('id') id: string,
    @Body() projectDTO: ProjectDTO,
  ): Promise<APIResponse<ProjectDTO>> {
    return this.serializeResponse(await this.projectService.updateProject(id, projectDTO));
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getAllProject(): Promise<APIResponse<ProjectDTO[]>> {
    return this.serializeResponse(await this.projectService.getAllProject());
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch(':id/stop')
  async stopProject(@Param('id') id: string): Promise<APIResponse<ProjectDTO>> {
    return this.serializeResponse(
      await this.projectService.stopProject(id, ProjectStatus.stopped),
    );
  }

  @UseGuards(AuthGuard('jwt'))
  @Patch(':id/complete')
  async completeProject(@Param('id') id: string): Promise<APIResponse<ProjectDTO>> {
    return this.serializeResponse(
      await this.projectService.stopProject(id, ProjectStatus.completed),
    );
  }

  private serializeResponse<T>(data: T): APIResponse<T> {
    return { data };
  }
}
