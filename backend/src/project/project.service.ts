import { Activity } from './../data/entities/activity.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Project } from '../data/entities/project.entity';
import { Repository } from 'typeorm';
import { ProjectDTO } from '../models/project/project.dto';
import { plainToClass } from 'class-transformer';
import { Member } from '../data/entities/member.entity';
import { User } from '../data/entities/user.entity';
import { Skill } from '../data/entities/skill.entity';
import { ActivityDTO } from '../models/project/activity.dto';
import { SkillDTO } from '../models/admin/skill.dto';
import { MemberDTO } from '../models/project/member.dto';
import { UserDTO } from '../models/user/user.dto';
import * as moment from 'moment';
import { ProjectStatus } from 'src/common/enums/project-status.enum';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepo: Repository<Project>,
    @InjectRepository(Member) private readonly memberRepo: Repository<Member>,
    @InjectRepository(Activity)
    private readonly activityRepo: Repository<Activity>,
    @InjectRepository(User) private readonly usersRepo: Repository<User>,
    @InjectRepository(Skill) private readonly skillsRepo: Repository<Skill>,
  ) {}

  async createProject(projectDTO: ProjectDTO): Promise<ProjectDTO> {
    let project = new Project();
    await this.populateNewProject(project, projectDTO);
    const savedProject = await this.projectRepo.save(project);
    const converted = plainToClass(ProjectDTO, savedProject, {
      excludeExtraneousValues: true,
    });
    return converted;
  }

  async getAllProject(): Promise<ProjectDTO[]> {
    let projects = await this.projectRepo.find();

    let converted = [];
    for (let project of projects) {
      converted.push(await this.convertProject(project));
    }
    return converted;
  }

  async getProject(id): Promise<ProjectDTO> {
    let project = await this.projectRepo.findOne({
      where: { id },
    });

    return await this.convertProject(project);
  }

  async updateProject(id: string, projectDTO: ProjectDTO): Promise<ProjectDTO> {
    if (+id !== +projectDTO.id) {
      return null;
    }
    let project = await this.projectRepo.findOne({
      where: { id },
    });
    await this.populateUpdateProject(project, projectDTO);
    const savedProject = await this.projectRepo.save(project);
    const converted = plainToClass(ProjectDTO, savedProject, {
      excludeExtraneousValues: true,
    });
    return converted;
  }

  // Convert Project to ProjectDTO, resolve all relations.
  async convertProject(project) {
    const converted = plainToClass(ProjectDTO, project, {
      excludeExtraneousValues: true,
    });

    converted.activities = [];
    let activities = await project.activities;
    for (let activity of activities) {
      let activityDTO = plainToClass(ActivityDTO, activity, {
        excludeExtraneousValues: true,
      });
      let activitySkill = await activity.skill;
      let skillDTO = plainToClass(SkillDTO, activitySkill, {
        excludeExtraneousValues: true,
      });
      activityDTO.skill = skillDTO;

      activityDTO.members = [];
      let members = await activity.members;
      for (let member of members) {
        let memberDTO = plainToClass(MemberDTO, member, {
          excludeExtraneousValues: true,
        });

        let memberUser = await member.user;
        let userDTO = plainToClass(UserDTO, memberUser, {
          excludeExtraneousValues: true,
        });
        memberDTO.user = userDTO;
        activityDTO.members.push(memberDTO);
      }

      converted.activities.push(activityDTO);
    }

    return converted;
  }

  async stopProject(id: string, stopType: ProjectStatus): Promise<ProjectDTO> {
    let project = await this.projectRepo.findOne({
      where: { id },
    });

    (await project.activities).forEach(async activity => {
      (await activity.members).forEach(member => {
        const today = moment();
        member.endDate = today.format('YYYY-MM-DD');
        this.memberRepo.save(member);
      });
    });

    project.status = stopType;
    const savedProject = await this.projectRepo.save(project);
    const converted = plainToClass(ProjectDTO, savedProject, {
      excludeExtraneousValues: true,
    });
    return converted;
  }

  async populateNewProject(project, projectDTO) {
    project.name = projectDTO.name;
    project.description = projectDTO.description;
    project.targetInDays = projectDTO.targetInDays;
    const today = moment();
    project.startDate = today.format('YYYY-MM-DD');

    //Create new activities and asign them to the project
    let savedActivities = [];
    for (let activityDTO of projectDTO.activities) {
      let newActivity = new Activity();
      newActivity.hours = activityDTO.hours;
      let skill = await this.skillsRepo.findOne({
        where: { id: activityDTO.skill.id },
      });
      newActivity.skill = Promise.resolve(skill);

      let savedMembers = [];
      for (let memberDTO of activityDTO.members) {
        let newMember = new Member();
        newMember.startDate = today.format('YYYY-MM-DD');
        newMember.hours = memberDTO.hours;

        let user = await this.usersRepo.findOne({
          where: { id: memberDTO.user.id },
        });
        newMember.user = Promise.resolve(user);
        const savedMember = await this.memberRepo.save(newMember);
        savedMembers.push(savedMember);
      }
      newActivity.members = Promise.resolve(savedMembers);
      const savedActivity = await this.activityRepo.save(newActivity);
      savedActivities.push(savedActivity);
    }
    project.activities = Promise.resolve(savedActivities);
  }

  async populateUpdateProject(project, projectDTO) {
    const today = moment();
    project.name = projectDTO.name;
    project.description = projectDTO.description;
    project.targetInDays = projectDTO.targetInDays;
    project.status = projectDTO.status;
    //Create new activities and asign them to the project
    let savedActivities = [];
    for (let activityDTO of projectDTO.activities) {
      let activity = await this.activityRepo.findOne({
        where: { id: activityDTO.id },
      });
      if (!activity) {
        activity = new Activity();
      }
      activity.hours = activityDTO.hours;
      let skill = await this.skillsRepo.findOne({
        where: { id: activityDTO.skill.id },
      });
      activity.skill = Promise.resolve(skill);
      activity.project = Promise.resolve(project);
      let savedMembers = [];
      for (let memberDTO of activityDTO.members) {
        let member = await this.memberRepo.findOne({
          where: { id: memberDTO.id },
        });
        if (!member) {
          member = new Member();
          member.startDate = today.format('YYYY-MM-DD');
        } else {
          // The daily hours input were changed, so store the current member as completed and create new one with the updated hours
          if (member.hours != memberDTO.hours) {
            const tomorow = moment();
            tomorow.add(1, 'days');
            if (member.startDate !== tomorow.format('YYYY-MM-DD')) {
              member.endDate = today.format('YYYY-MM-DD');
              const savedMember = await this.memberRepo.save(member);
              savedMembers.push(savedMember);
              member = new Member();
              member.startDate = tomorow.format('YYYY-MM-DD');
            }
          }
        }
        member.hours = memberDTO.hours;
        member.activity = Promise.resolve(activity);
        let user = await this.usersRepo.findOne({
          where: { id: memberDTO.user.id },
        });

        member.user = Promise.resolve(user);
        const savedMember = await this.memberRepo.save(member);
        savedMembers.push(savedMember);
      }
      activity.members = Promise.resolve(savedMembers);
      const savedActivity = await this.activityRepo.save(activity);
      savedActivities.push(savedActivity);
    }
    project.activities = Promise.resolve(savedActivities);
  }
}
