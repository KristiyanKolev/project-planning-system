import { Controller, UseGuards, Get, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { APIResponse } from '../models/user/api-response.dto';
import { UserDTO } from '../models/user/user.dto';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('user/:id')
  async getUserDetails(@Param('id') id: string): Promise<APIResponse<UserDTO>> {
    return this.serializeResponse(await this.userService.getUserDetails(id));
  }

  @UseGuards(AuthGuard('jwt'))
  @Get()
  async getAllUsers(): Promise<APIResponse<UserDTO[]>> {
    return this.serializeResponse(await this.userService.getAllUsers());
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/basic')
  async getAllBasicUsers(): Promise<APIResponse<UserDTO[]>> {
    return this.serializeResponse(await this.userService.getAllBasicUsers());
  }

  private serializeResponse<T>(data: T): APIResponse<T> {
    return { data };
  }
}
