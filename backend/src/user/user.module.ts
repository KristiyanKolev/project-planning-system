import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { User } from '../data/entities/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Skill } from '../data/entities/skill.entity';
import { Project } from '../data/entities/project.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Skill, Project])],
  controllers: [UserController],
  providers: [UserService, UserController],
  exports: [UserService, UserController],
})
export class UserModule {}
