import { ProjectDetailsComponent } from './projects/components/project-details/project-details.component';
import { ProjectsComponent } from './projects/components/projects/projects.component';
import { DashboardComponent } from './dashboard/components/dashboard/dashboard.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CreateProjectComponent } from './projects/components/create-project/create-project.component';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginFormComponent },
  { path: 'home', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'projects', component: ProjectsComponent, canActivate: [AuthGuard] },
  {
    path: 'projects/new',
    component: CreateProjectComponent,
    canActivate: [AuthGuard],
  },
  { path: 'projects/new/:id', component: CreateProjectComponent },
  { path: 'projects/:id', component: ProjectDetailsComponent },

  {
    path: 'employees',
    loadChildren: () => import('./employees/employees.module').then((m) => m.EmployeesModule),
  },
  {
    path: 'skills',
    loadChildren: () => import('./skills/skills.module').then((m) => m.SkillsModule),
  },

  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
