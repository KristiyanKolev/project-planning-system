import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoggedUserData } from '../../models/users/logged-user-data';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public loggedUserData: LoggedUserData;
  private subsciption: Subscription;

  public firstName: string;

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public logout() {
    this.authService.logout();
  }

  public navigateToProfile(): void {
    this.router.navigate(['employees', this.loggedUserData.id]);
  }

  public ngOnInit(): void {
    this.subsciption = this.authService.loggedUserData$.subscribe((loggedUserData) => {
      this.loggedUserData = loggedUserData;

      if (loggedUserData) {
        this.firstName = loggedUserData.firstName;
      }
    });
  }

  ngOnDestroy() {
    this.subsciption.unsubscribe();
  }
}
