import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent implements OnInit {
  public isFormSubmitted = false;

  public userCredentialsForm: FormGroup = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(50),
    ]),

    password: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(30),
    ]),
  });

  public constructor(
    private readonly notificationService: NotificationService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public login(email: string, password: string): void {
    this.isFormSubmitted = true;
    if (this.userCredentialsForm.invalid) {
      return;
    }
    this.authService.login(email, password).subscribe(
      () => {},
      () => this.notificationService.error('Unauthorized')
    );
  }

  public ngOnInit() {
    if (this.authService.getUserDataIfAuthenticated()) {
      this.router.navigate(['/home']);
    }
  }
}
