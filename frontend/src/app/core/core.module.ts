import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NotificationService } from './services/notification.service';
import { ValidationService } from './services/validation.service';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { UserService } from './services/user.service';
import { ProjectsService } from './services/projects.service';
import { SkillService } from './services/skill.service';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

@NgModule({
  providers: [
    NotificationService,
    ValidationService,
    AuthService,
    StorageService,
    UserService,
    ProjectsService,
    SkillService,
    AuthGuard,
    AdminGuard,
  ],
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core Module already initiated!');
    }
  }
}
