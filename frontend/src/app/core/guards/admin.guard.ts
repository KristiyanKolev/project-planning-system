import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UserRoles } from '../../models/common/enums/user-roles';

@Injectable()
export class AdminGuard implements CanActivate {
  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public canActivate(): boolean {
    const userData = this.authService.getUserDataIfAuthenticated();

    if (!(userData && userData.role === UserRoles.admin)) {
      this.router.navigate(['login']);

      return false;
    }

    return true;
  }
}
