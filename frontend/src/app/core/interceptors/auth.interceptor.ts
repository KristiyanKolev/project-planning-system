import { Observable } from 'rxjs';
import { StorageService } from '../services/storage.service';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthIntercepotor implements HttpInterceptor {
  public constructor(private readonly storage: StorageService) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const token = this.storage.getItem('token');
    const modifiedReq = request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`,
      },
    });
    return next.handle(modifiedReq);
  }
}
