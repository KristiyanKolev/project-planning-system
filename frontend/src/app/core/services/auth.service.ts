import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoggedUserData } from '../../models/users/logged-user-data';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificationService } from './notification.service';

@Injectable()
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<LoggedUserData>(
    this.getUserDataIfAuthenticated()
  );

  public constructor(
    private readonly router: Router,
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService,
    private readonly notificationService: NotificationService
  ) {}

  public get loggedUserData$(): Observable<LoggedUserData> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(userData: LoggedUserData): void {
    this.loggedUserDataSubject$.next(userData);
  }

  public getUserDataIfAuthenticated(): LoggedUserData {
    const token: string = this.storage.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }
    return token ? this.jwtService.decodeToken(token) : null;
  }

  public login(email: string, password: string): Observable<{ token: string }> {
    return this.http
      .post<{ token: string }>(`http://localhost:3000/auth/login`, {
        email,
        password,
      })
      .pipe(
        tap(({ token }) => {
          const userData: LoggedUserData = this.jwtService.decodeToken(token);
          if (userData.role !== 'BASIC') {
            this.storage.setItem('token', token);
            this.router.navigate(['/home']);
            this.emitUserData(userData);
            this.notificationService.success('Signed in successfully');
          } else {
            this.notificationService.error('Unauthorized');
          }
        })
      );
  }

  public logout(): void {
    this.storage.removeItem('token');
    this.router.navigate(['/login']);
    this.emitUserData(null);
    this.notificationService.success('Logged out successfully');
  }
}
