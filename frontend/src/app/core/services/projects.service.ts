import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIResponse } from 'src/app/models/common/api-response';
import { ProjectDTO } from 'src/app/models/projects/project.dto';
import { Observable } from 'rxjs';
import { CreateProjectDTO } from 'src/app/models/projects/create-project.dto';

@Injectable()
export class ProjectsService {
  constructor(private readonly httpClient: HttpClient) {}

  public getAllProjects(): Observable<APIResponse<ProjectDTO[]>> {
    return this.httpClient.get<APIResponse<ProjectDTO[]>>(`http://localhost:3000/projects`);
  }

  public createProject(project: CreateProjectDTO): Observable<APIResponse<ProjectDTO>> {
    return this.httpClient.post<APIResponse<ProjectDTO>>(
      `http://localhost:3000/projects`,
      project
    );
  }

  public projectDetails(id: string): Observable<APIResponse<ProjectDTO>> {
    return this.httpClient.get<APIResponse<ProjectDTO>>(
      `http://localhost:3000/projects/${id}`
    );
  }

  public editProject(id: string, project: ProjectDTO): Observable<APIResponse<ProjectDTO>> {
    return this.httpClient.patch<APIResponse<ProjectDTO>>(
      `http://localhost:3000/projects/${id}`,
      project
    );
  }

  public markProjectAsComplete(id: string): Observable<APIResponse<ProjectDTO>> {
    return this.httpClient.patch<APIResponse<ProjectDTO>>(
      `http://localhost:3000/projects/${id}/complete/`,
      { id }
    );
  }

  public stopProject(id: string): Observable<APIResponse<ProjectDTO>> {
    return this.httpClient.patch<APIResponse<ProjectDTO>>(
      `http://localhost:3000/projects/${id}/stop/`,
      { id }
    );
  }
}
