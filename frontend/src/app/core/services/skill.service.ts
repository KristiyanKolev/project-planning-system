import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { APIResponse } from '../../models/common/api-response';
import { SkillDTO } from '../../models/skills/skill.dto';
import { CreateSkillDTO } from '../../models/skills/create-skill.dto';

@Injectable()
export class SkillService {
  public constructor(private readonly httpClient: HttpClient) {}

  public getAllSkills(): Observable<APIResponse<SkillDTO[]>> {
    return this.httpClient.get<APIResponse<SkillDTO[]>>(`http://localhost:3000/skills`);
  }

  public createSkill(skill: CreateSkillDTO): Observable<APIResponse<SkillDTO>> {
    return this.httpClient.post<APIResponse<SkillDTO>>(
      `http://localhost:3000/admin/skills`,
      skill
    );
  }
}
