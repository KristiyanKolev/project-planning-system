import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { NotificationService } from '../notification.service';
import { StorageService } from '../storage.service';
import { AuthService } from '../auth.service';

describe('AuthService', () => {
  let notificationService: any;
  let storageService: any;
  let jwtService: any;
  let httpClient: any;
  let router: any;

  let authService: AuthService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    notificationService = {
      success: jest.fn(),
    };

    storageService = {
      getItem: jest.fn(),
      setItem: jest.fn(),
      removeItem: jest.fn(),
    };

    jwtService = {
      isTokenExpired: jest.fn(),
      decodeToken: jest.fn(),
    };

    router = {
      navigate: jest.fn(),
    };

    httpClient = {
      post: jest.fn(),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, JwtModule.forRoot({ config: {} })],
      providers: [AuthService, StorageService, NotificationService],
    })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .overrideProvider(Router, { useValue: router })
      .overrideProvider(HttpClient, { useValue: httpClient })
      .overrideProvider(StorageService, { useValue: storageService })
      .overrideProvider(JwtHelperService, { useValue: jwtService });

    authService = TestBed.inject(AuthService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(authService).toBeDefined();
  });
});
