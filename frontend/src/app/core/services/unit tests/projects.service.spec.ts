import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { ProjectsService } from '../projects.service';
import { of } from 'rxjs';

import { CreateProjectDTO } from '../../../models/projects/create-project.dto';
import { ProjectDTO } from '../../../models/projects/project.dto';

describe('ProjectsService', () => {
  let projectsService: ProjectsService;
  let httpClient: any;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get: jest.fn(),
      post: jest.fn(),
      patch: jest.fn(),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ProjectsService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    projectsService = TestBed.inject(ProjectsService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(projectsService).toBeDefined();
  });

  describe('getAllProjects()', () => {
    it('should call the httpClient.get() method once ' + 'with correct parameters', (done) => {
      // Arrange
      const url = `http://localhost:3000/projects`;
      const mockReturnValue = of('Mock Value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act & Assert
      projectsService.getAllProjects().subscribe(() => {
        expect(spy).toBeCalledTimes(1);
        expect(spy).toBeCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');

      jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act
      const result = projectsService.getAllProjects();

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('projectDetails()', () => {
    it('should call the httpClient.get() method once ' + 'with correct parameters', (done) => {
      // Arrange
      const projectId = '1';

      const url = `http://localhost:3000/projects/${projectId}`;
      const mockReturnValue = of('Mock Value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act & Assert
      projectsService.projectDetails(projectId).subscribe(() => {
        expect(spy).toBeCalledTimes(1);
        expect(spy).toBeCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const projectId = '1';

      jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act
      const result = projectsService.projectDetails(projectId);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('createProject()', () => {
    it(
      'should call the httpClient.post() method once ' + 'with correct parameters',
      (done) => {
        // Arrange
        const url = `http://localhost:3000/projects`;
        const mockReturnValue = of('Mock Value');
        const mockProject = new CreateProjectDTO();

        const spy = jest.spyOn(httpClient, 'post').mockReturnValue(mockReturnValue);

        // Act & Assert
        projectsService.createProject(mockProject).subscribe(() => {
          expect(spy).toBeCalledTimes(1);
          expect(spy).toBeCalledWith(url, mockProject);

          done();
        });
      }
    );

    it('should return the result from the httpClient.post() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const mockProject = new CreateProjectDTO();

      jest.spyOn(httpClient, 'post').mockReturnValue(mockReturnValue);

      // Act
      const result = projectsService.createProject(mockProject);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('editProject()', () => {
    it(
      'should call the httpClient.patch() method once ' + 'with correct parameters',
      (done) => {
        // Arrange
        const mockProject = new ProjectDTO();
        const projectId = '1';

        const url = `http://localhost:3000/projects/${projectId}`;
        const mockReturnValue = of('Mock Value');

        const spy = jest.spyOn(httpClient, 'patch').mockReturnValue(mockReturnValue);

        // Act & Assert
        projectsService.editProject(projectId, mockProject).subscribe(() => {
          expect(spy).toBeCalledTimes(1);
          expect(spy).toBeCalledWith(url, mockProject);

          done();
        });
      }
    );

    it('should return the result from the httpClient.patch() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const mockProject = new ProjectDTO();
      const projectId = '1';

      jest.spyOn(httpClient, 'patch').mockReturnValue(mockReturnValue);

      // Act
      const result = projectsService.editProject(projectId, mockProject);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });
});
