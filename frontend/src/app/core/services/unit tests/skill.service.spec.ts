import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { SkillService } from '../skill.service';
import { of } from 'rxjs';

import { CreateSkillDTO } from '../../../models/skills/create-skill.dto';

describe('SkillService', () => {
  let skillService: SkillService;
  let httpClient: any;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get: jest.fn(),
      post: jest.fn(),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [SkillService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    skillService = TestBed.inject(SkillService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(skillService).toBeDefined();
  });

  describe('getAllSkills()', () => {
    it('should call the httpClient.get() method once ' + 'with correct parameters', (done) => {
      // Arrange
      const url = `http://localhost:3000/skills`;
      const mockReturnValue = of('Mock Value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act & Assert
      skillService.getAllSkills().subscribe(() => {
        expect(spy).toBeCalledTimes(1);
        expect(spy).toBeCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');

      jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act
      const result = skillService.getAllSkills();

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('createSkill()', () => {
    it(
      'should call the httpClient.post() method once ' + 'with correct parameters',
      (done) => {
        // Arrange
        const url = `http://localhost:3000/admin/skills`;
        const mockReturnValue = of('Mock Value');
        const mockSkill = new CreateSkillDTO();

        const spy = jest.spyOn(httpClient, 'post').mockReturnValue(mockReturnValue);

        // Act & Assert
        skillService.createSkill(mockSkill).subscribe(() => {
          expect(spy).toBeCalledTimes(1);
          expect(spy).toBeCalledWith(url, mockSkill);

          done();
        });
      }
    );

    it('should return the result from the httpClient.post() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const mockSkill = new CreateSkillDTO();

      jest.spyOn(httpClient, 'post').mockReturnValue(mockReturnValue);

      // Act
      const result = skillService.createSkill(mockSkill);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });
});
