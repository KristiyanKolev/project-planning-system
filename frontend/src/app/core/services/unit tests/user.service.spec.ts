import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { UserService } from '../user.service';
import { of } from 'rxjs';

import { CreateEmployeeDTO } from '../../../models/users/create-employee.dto';
import { UserDTO } from '../../../models/users/user.dto';

describe('UserService', () => {
  let userService: UserService;
  let httpClient: any;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get: jest.fn(),
      post: jest.fn(),
      patch: jest.fn(),
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [UserService],
    }).overrideProvider(HttpClient, { useValue: httpClient });

    userService = TestBed.inject(UserService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(userService).toBeDefined();
  });

  describe('getAllUsers()', () => {
    it('should call the httpClient.get() method once ' + 'with correct parameters', (done) => {
      // Arrange
      const url = `http://localhost:3000/users`;
      const mockReturnValue = of('Mock Value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act & Assert
      userService.getAllUsers().subscribe(() => {
        expect(spy).toBeCalledTimes(1);
        expect(spy).toBeCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');

      jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act
      const result = userService.getAllUsers();

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('getAllBasicUsers()', () => {
    it('should call the httpClient.get() method once ' + 'with correct parameters', (done) => {
      // Arrange
      const url = `http://localhost:3000/users/basic`;
      const mockReturnValue = of('Mock Value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act & Assert
      userService.getAllBasicUsers().subscribe(() => {
        expect(spy).toBeCalledTimes(1);
        expect(spy).toBeCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');

      jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act
      const result = userService.getAllBasicUsers();

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('getUserDetails()', () => {
    it('should call the httpClient.get() method once ' + 'with correct parameters', (done) => {
      // Arrange
      const userId = 1;

      const url = `http://localhost:3000/users/user/${userId}`;
      const mockReturnValue = of('Mock Value');

      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act & Assert
      userService.getUserDetails(userId).subscribe(() => {
        expect(spy).toBeCalledTimes(1);
        expect(spy).toBeCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient.get() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const userId = 1;

      jest.spyOn(httpClient, 'get').mockReturnValue(mockReturnValue);

      // Act
      const result = userService.getUserDetails(userId);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('createUser()', () => {
    it(
      'should call the httpClient.post() method once ' + 'with correct parameters',
      (done) => {
        // Arrange
        const url = `http://localhost:3000/admin/users`;
        const mockReturnValue = of('Mock Value');
        const mockEmployee = new CreateEmployeeDTO();

        const spy = jest.spyOn(httpClient, 'post').mockReturnValue(mockReturnValue);

        // Act & Assert
        userService.createUser(mockEmployee).subscribe(() => {
          expect(spy).toBeCalledTimes(1);
          expect(spy).toBeCalledWith(url, mockEmployee);

          done();
        });
      }
    );

    it('should return the result from the httpClient.post() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const mockEmployee = new CreateEmployeeDTO();

      jest.spyOn(httpClient, 'post').mockReturnValue(mockReturnValue);

      // Act
      const result = userService.createUser(mockEmployee);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });

  describe('updateUser()', () => {
    it(
      'should call the httpClient.patch() method once ' + 'with correct parameters',
      (done) => {
        // Arrange
        const mockEmployee = new UserDTO();
        mockEmployee.id = '1';

        const url = `http://localhost:3000/admin/users/${mockEmployee.id}`;
        const mockReturnValue = of('Mock Value');

        const spy = jest.spyOn(httpClient, 'patch').mockReturnValue(mockReturnValue);

        // Act & Assert
        userService.updateUser(mockEmployee).subscribe(() => {
          expect(spy).toBeCalledTimes(1);
          expect(spy).toBeCalledWith(url, mockEmployee);

          done();
        });
      }
    );

    it('should return the result from the httpClient.patch() method', () => {
      // Arrange
      const mockReturnValue = of('Mock Value');
      const mockEmployee = new UserDTO();

      jest.spyOn(httpClient, 'patch').mockReturnValue(mockReturnValue);

      // Act
      const result = userService.updateUser(mockEmployee);

      // Assert
      expect(result).toEqual(mockReturnValue);
    });
  });
});
