import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../../models/users/user.dto';
import { APIResponse } from '../../models/common/api-response';
import { CreateEmployeeDTO } from '../../models/users/create-employee.dto';

@Injectable()
export class UserService {
  public constructor(private readonly httpClient: HttpClient) {}

  public getAllUsers(): Observable<APIResponse<UserDTO[]>> {
    return this.httpClient.get<APIResponse<UserDTO[]>>(`http://localhost:3000/users`);
  }

  public getAllBasicUsers(): Observable<APIResponse<UserDTO[]>> {
    return this.httpClient.get<APIResponse<UserDTO[]>>(`http://localhost:3000/users/basic`);
  }

  public getUserDetails(userId: number): Observable<APIResponse<UserDTO>> {
    return this.httpClient.get<APIResponse<UserDTO>>(
      `http://localhost:3000/users/user/${userId}`
    );
  }

  public createUser(user: CreateEmployeeDTO): Observable<APIResponse<UserDTO>> {
    return this.httpClient.post<APIResponse<UserDTO>>(
      `http://localhost:3000/admin/users`,
      user
    );
  }

  public updateUser(user: UserDTO): Observable<APIResponse<UserDTO>> {
    return this.httpClient.patch<APIResponse<UserDTO>>(
      `http://localhost:3000/admin/users/${user.id}`,
      user
    );
  }
}
