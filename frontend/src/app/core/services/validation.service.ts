import { Injectable } from '@angular/core';
import { NotificationService } from './notification.service';
import { CreateEmployeeDTO } from '../../models/users/create-employee.dto';
import { SkillDTO } from '../../models/skills/skill.dto';
import { UserDTO } from '../../models/users/user.dto';

@Injectable()
export class ValidationService {
  public constructor(private readonly notificationService: NotificationService) {}

  public validateNewSkill(skillName: string, skillsList: SkillDTO[]): boolean {
    if (!skillName) {
      this.notificationService.error('Skill field is empty');
      return false;
    }

    skillName = skillName.toLowerCase().trim();
    const foundSkill: SkillDTO = skillsList.find((s) => s.skill === skillName);

    if (foundSkill) {
      this.notificationService.error('This skill already exists');
      return false;
    }
    return true;
  }

  public validateNewEmployee(employee: CreateEmployeeDTO, employeesList: UserDTO[]): boolean {
    if (
      !employee.firstName ||
      !employee.surname ||
      !employee.position ||
      !employee.email ||
      !employee.password ||
      !employee.role ||
      !employee.managedBy.id
    ) {
      this.notificationService.error('Some fields are empty');
      return false;
    }

    const emailCheck: boolean = employeesList.some((u) => u.email === employee.email);

    if (emailCheck) {
      this.notificationService.error('This email is already taken');
      return false;
    }

    if (employee.email.length < 5) {
      this.notificationService.error('The email should be at least 5 characters');
      return false;
    }
    if (employee.email.length > 50) {
      this.notificationService.error('The email should be maximum 50 characters');
      return false;
    }
    if (employee.password.length < 5) {
      this.notificationService.error('The password should be at least 5 characters');
      return false;
    }
    if (employee.password.length > 30) {
      this.notificationService.error('The password should be maximum 30 characters');
      return false;
    }
    return true;
  }
}
