import { ProjectDTO } from 'src/app/models/projects/project.dto';
import { ProjectsService } from './../../../core/services/projects.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-manager-active-projects',
  templateUrl: './manager-active-projects.component.html',
  styleUrls: ['./manager-active-projects.component.css'],
})
export class ManagerActiveProjectsComponent implements OnInit {
  private _keyword: string;
  public loggedUserId: string;
  public currentActiveProjects: ProjectDTO[];
  public filterProjectsByKeyword: ProjectDTO[];
  public searchType: string = undefined;

  constructor(
    private readonly authService: AuthService,
    private readonly projectService: ProjectsService
  ) {}

  get keyword(): string {
    return this._keyword;
  }

  set keyword(val: string) {
    this._keyword = val;
    this.filterProjectsByKeyword = this.filterByKeyword(val, this.searchType);
  }

  filterByKeyword(searchString: string, searchType: string): ProjectDTO[] {
    if (searchType === undefined || searchType === null) {
      return this.currentActiveProjects;
    } else if (searchType === 'keyword') {
      return this.currentActiveProjects.filter(
        (project) =>
          project.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1 ||
          project.description.toLowerCase().indexOf(searchString.toLowerCase()) !== -1 ||
          project.status.toLowerCase().indexOf(searchString.toLowerCase()) !== -1
      );
    } else if (searchType === 'status') {
      return this.currentActiveProjects.filter(
        (project) => project.status.toLowerCase().indexOf(searchString.toLowerCase()) !== -1
      );
    } else if (searchType === 'skill') {
      return this.currentActiveProjects.filter((project) => {
        return project.activities.find((skill) => {
          return skill.skill.skill.toLowerCase().indexOf(searchString.toLowerCase()) !== -1;
        });
      });
    }
  }

  resetFilter(): void {
    this.searchType = undefined;
  }

  setToStatus(): void {
    this.searchType = 'status';
  }
  setToSkill(): void {
    this.searchType = 'skill';
  }

  setToKeyword(): void {
    this.searchType = 'keyword';
  }

  ngOnInit(): void {
    this.authService.loggedUserData$.subscribe((userData) => {
      this.loggedUserId = userData.id;

      return;
    });

    this.projectService.getAllProjects().subscribe((data) => {
      this.currentActiveProjects = data.data.filter((project) => {
        return (
          project.status === 'INPROGRESS' &&
          project.activities.find((activity) => {
            return activity.members.find((member) => {
              return member.user.id === this.loggedUserId;
            });
          })
        );
      });

      this.filterProjectsByKeyword = this.currentActiveProjects;
      return;
    });
  }
}
