import { SubordinatesDTO } from './../../../models/users/subordinates.dto';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-subordinates',
  templateUrl: './manager-subordinates.component.html',
  styleUrls: ['./manager-subordinates.component.css'],
})
export class ManagerSubordinatesComponent implements OnInit {
  private loggedUserId: string;
  public subordinates: SubordinatesDTO[];

  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public navigateToProfile(subordinateId: number): void {
    this.router.navigate(['employees', subordinateId]);
  }

  ngOnInit(): void {
    this.authService.loggedUserData$.subscribe((userData) => {
      this.loggedUserId = userData.id;
    });

    this.userService.getUserDetails(+this.loggedUserId).subscribe((userInfo) => {
      this.subordinates = userInfo.data.subordinates;
      return this.subordinates[0].firstName;
    });
  }
}
