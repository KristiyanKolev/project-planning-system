import { LoggedUserData } from 'src/app/models/users/logged-user-data';
import { UserService } from '../../../core/services/user.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logged-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
})
export class LoggedUserDetailsComponent implements OnInit {
  public loggedUserData: LoggedUserData;
  public loggedUserId: string;
  public firstName: string;
  public surname: string;
  public position: string;
  public role: string;
  public directManager: string;

  public constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  public ngOnInit(): void {
    this.authService.loggedUserData$.subscribe((loggedUserData) => {
      this.loggedUserData = loggedUserData;
      this.loggedUserId = loggedUserData.id;
    });
    this.userService.getUserDetails(+this.loggedUserId).subscribe((userData) => {
      this.firstName = userData.data.firstName;
      this.surname = userData.data.surname;
      this.position = userData.data.position;
      this.role = userData.data.role;
      if (userData.data.managedBy !== undefined) {
        this.directManager =
          userData.data.managedBy.firstName + ' ' + userData.data.managedBy.surname;
      } else {
        this.directManager = 'self-managed';
      }
    });
  }
}
