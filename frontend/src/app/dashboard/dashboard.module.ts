import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LoggedUserDetailsComponent } from './components/user-details/user-details.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ManagerActiveProjectsComponent } from './components/manager-active-projects/manager-active-projects.component';
import { ManagerSubordinatesComponent } from './components/manager-subordinates/manager-subordinates.component';

@NgModule({
  declarations: [
    LoggedUserDetailsComponent,
    DashboardComponent,
    ManagerActiveProjectsComponent,
    ManagerSubordinatesComponent,
  ],
  imports: [SharedModule],
  exports: [],
})
export class DashboardModule {}
