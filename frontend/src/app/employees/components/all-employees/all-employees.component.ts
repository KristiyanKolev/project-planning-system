import { Component, Input } from '@angular/core';
import { UserDTO } from '../../../models/users/user.dto';
import { Router } from '@angular/router';
import { LoggedUserData } from '../../../models/users/logged-user-data';

@Component({
  selector: 'app-all-employees',
  templateUrl: './all-employees.component.html',
  styleUrls: ['./all-employees.component.css'],
})
export class AllEmployeesComponent {
  @Input() public loggedUser: LoggedUserData;
  @Input() public employees: UserDTO[];
  @Input() public header: string;

  public constructor(private readonly router: Router) {}

  public navigateToEmployeeForm(employeeId: number): void {
    this.router.navigate(['employees/form', employeeId]);
  }

  public navigateToProfile(employeeId: number): void {
    this.router.navigate(['employees', employeeId]);
  }
}
