import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserDTO } from '../../../models/users/user.dto';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css'],
})
export class EmployeeDetailsComponent implements OnInit {
  @Input() public employeeDetails: UserDTO;
  public directManager: string;

  public constructor(private readonly router: Router) {}

  public filterBySkill(skillName: string): void {
    this.router.navigate(['employees'], {
      queryParams: { skill: skillName },
    });
  }

  public ngOnInit(): void {
    if (this.employeeDetails.managedBy) {
      const fullName: string = this.employeeDetails.managedBy.firstName.concat(
        ' ',
        this.employeeDetails.managedBy.surname
      );

      this.directManager = fullName;
    } else {
      this.directManager = 'self-managed';
    }
  }
}
