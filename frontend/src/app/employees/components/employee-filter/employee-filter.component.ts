import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-employee-filter',
  templateUrl: './employee-filter.component.html',
  styleUrls: ['./employee-filter.component.css'],
})
export class EmployeeFilterComponent {
  @Output() public readonly filterEmitter: EventEmitter<string> = new EventEmitter();

  public onSearchInput(searchInput: string) {
    this.filterEmitter.emit(searchInput);
  }

  public onFilterClick(filterKeyword: string): void {
    this.filterEmitter.emit(filterKeyword);
  }
}
