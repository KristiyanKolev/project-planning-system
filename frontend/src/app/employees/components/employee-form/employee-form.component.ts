import { Component, OnInit } from '@angular/core';
import { UserRoles } from '../../../models/common/enums/user-roles';
import { UserDTO } from '../../../models/users/user.dto';
import { SkillDTO } from '../../../models/skills/skill.dto';
import { UserService } from '../../../core/services/user.service';
import { SkillService } from '../../../core/services/skill.service';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CreateEmployeeDTO } from '../../../models/users/create-employee.dto';
import { UpdateEmployee } from '../../../models/users/update-employee';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../../core/services/notification.service';
import { ValidationService } from '../../../core/services/validation.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css'],
})
export class EmployeeFormComponent implements OnInit {
  public managersList: UserDTO[];
  public employeesList: UserDTO[];
  public basicSkillsList: SkillDTO[];
  public managementSkill: SkillDTO;
  public employeeForm: FormGroup;

  public employeeDetails: UserDTO;
  public paramsPath = 'formId';
  public formId: string;

  public constructor(
    private readonly validationService: ValidationService,
    private readonly notificationService: NotificationService,
    private readonly userService: UserService,
    private readonly skillService: SkillService,
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) {}

  public get skillsArray(): FormArray {
    return this.employeeForm.get('skills') as FormArray;
  }

  public createSkill(): FormGroup {
    return this.formBuilder.group({
      id: [null, Validators.required],
    });
  }

  public addSkill(): void {
    this.skillsArray.push(this.createSkill());
  }

  public removeSkill(index: number): void {
    this.skillsArray.removeAt(index);
  }

  public clearSkills(): void {
    this.skillsArray.clear();
  }

  public filterSkills(skills: SkillDTO[]): SkillDTO[] {
    const skillIds: number[] = skills.map((s) => s.id);

    const filteredSkills: SkillDTO[] = this.basicSkillsList.filter(
      (s) => !skillIds.includes(s.id)
    );
    return filteredSkills;
  }

  public registerNewEmployee(newEmployee: CreateEmployeeDTO): void {
    if (!this.validationService.validateNewEmployee(newEmployee, this.employeesList)) {
      return;
    }

    if (newEmployee.role !== 'BASIC') {
      newEmployee.skills = [{ id: this.managementSkill.id }];
    }

    this.userService.createUser(newEmployee).subscribe(() => {
      this.router.navigate(['employees']);
      this.notificationService.success('Employee registration successful');
    });
  }

  public updateEmployee(employee: UpdateEmployee): void {
    if (!employee.firstName || !employee.surname || !employee.position) {
      this.notificationService.error('Some required fields are empty');
      return;
    }

    employee.skills = [...this.employeeDetails.skills, ...employee.skills];
    const updatedEmpoyee: UserDTO = { ...this.employeeDetails, ...employee };

    this.userService.updateUser(updatedEmpoyee).subscribe(() => {
      this.router.navigate(['employees', updatedEmpoyee.id]);
      this.notificationService.success('Employee successfully updated');
    });
  }

  public ngOnInit(): void {
    this.formId = this.activatedRoute.snapshot.params[this.paramsPath];

    if (this.formId) {
      this.userService.getUserDetails(+this.formId).subscribe((userDetails) => {
        this.employeeDetails = userDetails.data;

        this.employeeForm = this.formBuilder.group({
          firstName: [this.employeeDetails.firstName, Validators.required],
          surname: [this.employeeDetails.surname, Validators.required],
          position: [this.employeeDetails.position, Validators.required],
          skills: this.formBuilder.array([]),
        });
      });
    } else {
      this.employeeForm = this.formBuilder.group({
        firstName: [null, Validators.required],
        surname: [null, Validators.required],
        position: [null, Validators.required],
        email: [null, Validators.required],
        password: [null, Validators.required],
        role: [null, Validators.required],
        managedBy: this.formBuilder.group({
          id: [null, Validators.required],
        }),
        skills: this.formBuilder.array([]),
      });
    }

    this.userService.getAllUsers().subscribe((allUsers) => {
      this.employeesList = allUsers.data;

      this.managersList = allUsers.data.filter((u) => u.role !== UserRoles.basic);
    });

    this.skillService.getAllSkills().subscribe((allSkills) => {
      const managment: SkillDTO = allSkills.data.find((s) => s.skill === 'management');
      this.managementSkill = managment;

      this.basicSkillsList = allSkills.data.filter((s) => s.skill !== 'management');
    });
  }
}
