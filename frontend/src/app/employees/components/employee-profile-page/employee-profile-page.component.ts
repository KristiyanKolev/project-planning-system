import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserDTO } from '../../../models/users/user.dto';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-employee-profile-page',
  templateUrl: './employee-profile-page.component.html',
  styleUrls: ['./employee-profile-page.component.css'],
})
export class EmployeeProfilePageComponent implements OnInit {
  public employeeDetails: UserDTO;
  public paramsPath = 'employeeId';

  public constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly userService: UserService
  ) {}

  public ngOnInit(): void {
    const employeeId: number = +this.activatedRoute.snapshot.params[this.paramsPath];

    this.userService
      .getUserDetails(employeeId)
      .subscribe((userDetails) => (this.employeeDetails = userDetails.data));
  }
}
