import { ProjectDTO } from '../../../models/projects/project.dto';
import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from '../../../models/users/user.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-projects',
  templateUrl: './employee-projects.component.html',
  styleUrls: ['./employee-projects.component.css'],
})
export class EmployeeProjectsComponent implements OnInit {
  @Input() public employeeDetails: UserDTO;
  public currentProjects: ProjectDTO[];

  public constructor(private readonly router: Router) {}

  public navigateToProject(projectId: number): void {
    this.router.navigate(['projects', projectId]);
  }

  public ngOnInit(): void {
    const mappedProjects: ProjectDTO[] = this.employeeDetails.projectsMember.map(
      (p) => p.activity.project
    );

    this.currentProjects = mappedProjects.filter((p) => p.status === 'INPROGRESS');
  }
}
