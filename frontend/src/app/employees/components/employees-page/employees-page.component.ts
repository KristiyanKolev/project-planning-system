import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoggedUserData } from '../../../models/users/logged-user-data';
import { AuthService } from '../../../core/services/auth.service';
import { UserService } from '../../../core/services/user.service';
import { UserDTO } from '../../../models/users/user.dto';
import { UserRoles } from '../../../models/common/enums/user-roles';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employees-page',
  templateUrl: './employees-page.component.html',
  styleUrls: ['./employees-page.component.css'],
})
export class EmployeesPageComponent implements OnInit, OnDestroy {
  private loggedUserSubsciption: Subscription;
  public loggedUserData: LoggedUserData;
  public employeesList: UserDTO[];
  public filterKeyword = 'All Employees';

  public constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  filterEmployees(keyword: string): void {
    this.userService.getAllUsers().subscribe((allUsers) => {
      this.filterKeyword = keyword;

      if (keyword === 'All Employees') {
        this.employeesList = allUsers.data;

        return;
      }
      if (keyword === 'All Managers') {
        this.employeesList = allUsers.data.filter((u) => u.role !== UserRoles.basic);

        return;
      }
      if (keyword === 'My Subordinates') {
        this.userService.getUserDetails(+this.loggedUserData.id).subscribe((userDetails) => {
          const subordinatesIds = userDetails.data.subordinates.map((s) => s.id);
          this.employeesList = allUsers.data.filter((u) => subordinatesIds.includes(+u.id));

          return;
        });
      }

      this.employeesList = allUsers.data.filter((u) => {
        if (
          u.firstName.toLowerCase().includes(keyword.toLowerCase()) ||
          u.surname.toLowerCase().includes(keyword.toLowerCase()) ||
          u.position.toLowerCase().includes(keyword.toLowerCase()) ||
          u.email.toLowerCase().includes(keyword.toLowerCase()) ||
          u.role.toLowerCase().includes(keyword.toLowerCase()) ||
          u.skills.some((s) => s.skill.toLowerCase().includes(keyword.toLowerCase()))
        ) {
          return true;
        }
        return false;
      });
    });
  }

  public ngOnInit(): void {
    const skillName: string = this.activatedRoute.snapshot.queryParams.skill;

    this.loggedUserSubsciption = this.authService.loggedUserData$.subscribe(
      (loggedUserData) => {
        this.loggedUserData = loggedUserData;

        this.userService.getAllUsers().subscribe((response) => {
          if (skillName) {
            this.employeesList = response.data.filter((u) =>
              u.skills.find((s) => s.skill === skillName)
            );

            this.filterKeyword = skillName;
          } else {
            this.employeesList = response.data;
          }
        });
      }
    );
  }

  ngOnDestroy() {
    this.loggedUserSubsciption.unsubscribe();
  }
}
