import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';
import { AdminGuard } from '../core/guards/admin.guard';
import { NgModule } from '@angular/core';

import { EmployeesPageComponent } from './components/employees-page/employees-page.component';
import { EmployeeProfilePageComponent } from './components/employee-profile-page/employee-profile-page.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeesPageComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'form',
    component: EmployeeFormComponent,
    canActivate: [AdminGuard],
  },
  {
    path: 'form/:formId',
    component: EmployeeFormComponent,
    canActivate: [AdminGuard],
  },
  {
    path: ':employeeId',
    component: EmployeeProfilePageComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeesRoutingModule {}
