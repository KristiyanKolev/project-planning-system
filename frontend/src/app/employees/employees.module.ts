import { EmployeesRoutingModule } from './employees-routing.module';
import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';

import { EmployeeProfilePageComponent } from './components/employee-profile-page/employee-profile-page.component';
import { EmployeesPageComponent } from './components/employees-page/employees-page.component';
import { AllEmployeesComponent } from './components/all-employees/all-employees.component';
import { EmployeeFilterComponent } from './components/employee-filter/employee-filter.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { EmployeeProjectsComponent } from './components/employee-projects/employee-projects.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';

@NgModule({
  declarations: [
    EmployeeProfilePageComponent,
    EmployeesPageComponent,
    AllEmployeesComponent,
    EmployeeProfilePageComponent,
    EmployeeFilterComponent,
    EmployeeDetailsComponent,
    EmployeeProjectsComponent,
    EmployeeFormComponent,
  ],
  imports: [EmployeesRoutingModule, SharedModule],
})
export class EmployeesModule {}
