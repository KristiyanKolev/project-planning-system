export enum UserRoles {
  basic = 'BASIC',
  manager = 'MANAGER',
  admin = 'ADMIN',
}
