import { UserDTO } from '../users/user.dto';

export class MemberDTO {
  public hours: number;
  public user: UserDTO;
}
