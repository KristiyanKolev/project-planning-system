import { MemberDTO } from './../members/member.dto';
import { SkillDTO } from './../skills/skill.dto';

export class ActivitiesDTO {
  public hours: number;
  public skill: SkillDTO;
  public members: MemberDTO;
}
