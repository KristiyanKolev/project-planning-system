import { ProjectDTO } from './project.dto';

export class ActivityDTO {
  public hours: number;
  public project: ProjectDTO;
}
