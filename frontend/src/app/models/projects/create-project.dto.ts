import { ProjectActivitiesDTO } from './project-activities.dto';

export class CreateProjectDTO {
  public name: string;
  public description: string;
  public targetInDays: number;
  public activities: ProjectActivitiesDTO[];
}
