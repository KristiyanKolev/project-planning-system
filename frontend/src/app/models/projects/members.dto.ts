import { UserDTO } from '../users/user.dto';

export class MembersDTO {
  public id: string;
  public hours: number;
  public user: UserDTO;
  public startDate: Date;
  public endDate: Date;
}
