import { SkillDTO } from '../skills/skill.dto';
import { MembersDTO } from './members.dto';

export class ProjectActivitiesDTO {
  public id: string;
  public hours: number;
  public skill: SkillDTO;
  public members: MembersDTO[];
}
