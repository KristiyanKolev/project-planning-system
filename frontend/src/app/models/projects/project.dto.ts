import { ProjectStatus } from '../common/enums/project-status';
import { ProjectsMemberDTO } from './projectsMember.dto';
import { ProjectActivitiesDTO } from './project-activities.dto';

export class ProjectDTO {
  public id: number;
  public name: string;
  public description: string;
  public targetInDays: number;
  public status: ProjectStatus;
  public startDate: Date;
  public activities: ProjectActivitiesDTO[];
  public projectsMember: ProjectsMemberDTO[];
}
