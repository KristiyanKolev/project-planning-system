import { ActivityDTO } from './activity.dto';

export class ProjectsMemberDTO {
  public id: number;
  public hours: number;
  public startDate: string;
  public endDate: string;
  public activity: ActivityDTO;
}
