import { UserRoles } from '../common/enums/user-roles';
import { IdDTO } from '../common/id.dto';

export class CreateEmployeeDTO {
  public firstName: string;
  public surname: string;
  public email: string;
  public password: string;
  public position: string;
  public role: UserRoles;
  public managedBy: IdDTO;
  public skills: IdDTO[];
}
