import { UserRoles } from '../common/enums/user-roles';
import { ProjectsMemberDTO } from '../projects/projectsMember.dto';


export class LoggedUserData {
  public id: string;
  public firstName: string;
  public surname: string;
  public email: string;
  public role: UserRoles;
  public position: string;
}
