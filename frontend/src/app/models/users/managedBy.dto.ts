import { UserRoles } from '../common/enums/user-roles';

export class ManagedByDTO {
  public id: number;
  public firstName: string;
  public surname: string;
  public email: string;
  public position: string;
  public role: UserRoles;
}
