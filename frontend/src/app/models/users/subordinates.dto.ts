import { UserRoles } from '../common/enums/user-roles';

export class SubordinatesDTO {
    public id: number;
    public firstName: string;
    public surname: string;
    public role: UserRoles;
    public position: string;
    public email: string;
}
