import { SkillDTO } from '../skills/skill.dto';

export class UpdateEmployee {
  public firstName: string;
  public surname: string;
  public position: string;
  public skills: SkillDTO[];
}
