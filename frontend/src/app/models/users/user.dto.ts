import { ProjectsMemberDTO } from './../projects/projectsMember.dto';
import { SubordinatesDTO } from './subordinates.dto';
import { ManagedByDTO } from './managedBy.dto';
import { UserRoles } from '../common/enums/user-roles';
import { SkillDTO } from '../skills/skill.dto';

export class UserDTO {
  public id: string;
  public firstName: string;
  public surname: string;
  public email: string;
  public position: string;
  public role: UserRoles;
  public skills: SkillDTO[];
  public managedBy: ManagedByDTO;
  public projectsMember: ProjectsMemberDTO[];
  public subordinates: SubordinatesDTO[];
}
