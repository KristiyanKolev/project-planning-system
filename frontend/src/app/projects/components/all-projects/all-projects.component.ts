import { UserService } from './../../../core/services/user.service';
import { ProjectsService } from './../../../core/services/projects.service';
import { Component, OnInit } from '@angular/core';
import { ProjectDTO } from 'src/app/models/projects/project.dto';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-all-projects',
  templateUrl: './all-projects.component.html',
  styleUrls: ['./all-projects.component.css'],
})
export class AllProjectsComponent implements OnInit {
  private _keyword: string;
  public allCompanyProjects: ProjectDTO[];
  public filterProjectsByKeyword: ProjectDTO[];
  public searchType: string = undefined;
  public loggedUserId: string;
  public loggedUserCurrentHours: number;

  constructor(
    private readonly projectService: ProjectsService,
    private readonly auth: AuthService,
    private readonly userService: UserService
  ) {}

  get keyword(): string {
    return this._keyword;
  }

  set keyword(val: string) {
    this._keyword = val;
    this.filterProjectsByKeyword = this.filterByKeyword(val, this.searchType);
  }

  filterByKeyword(searchString: string, searchType: string): ProjectDTO[] {
    if (searchType === undefined || searchType === null) {
      return this.allCompanyProjects;
    } else if (searchType === 'keyword') {
      return this.allCompanyProjects.filter(
        (project) =>
          project.name.toLowerCase().indexOf(searchString.toLowerCase()) !== -1 ||
          project.description.toLowerCase().indexOf(searchString.toLowerCase()) !== -1 ||
          project.status.toLowerCase().indexOf(searchString.toLowerCase()) !== -1
      );
    } else if (searchType === 'status') {
      return this.allCompanyProjects.filter(
        (project) => project.status.toLowerCase().indexOf(searchString.toLowerCase()) !== -1
      );
    } else if (searchType === 'skill') {
      return this.allCompanyProjects.filter((project) => {
        return project.activities.find((skill) => {
          return skill.skill.skill.toLowerCase().indexOf(searchString.toLowerCase()) !== -1;
        });
      });
    } else if (searchType === 'owner') {
      return this.allCompanyProjects.filter((project) => {
        const x = project.activities.find((activity) => {
          return activity.members.find((member) => {
            return member.user.id === this.loggedUserId;
          });
        });
        return x;
      });
    }
  }

  resetFilter(): void {
    this.searchType = undefined;
  }

  setToStatus(): void {
    this.searchType = 'status';
  }
  setToSkill(): void {
    this.searchType = 'skill';
  }
  setToOwner(): void {
    this.searchType = 'owner';
    this.filterProjectsByKeyword = this.filterByKeyword('', this.searchType);
  }

  setToKeyword(): void {
    this.searchType = 'keyword';
  }

  ngOnInit(): void {
    this.auth.loggedUserData$.subscribe((data) => {
      this.loggedUserId = data.id;

      return;
    });
    this.projectService.getAllProjects().subscribe((data) => {
      this.allCompanyProjects = data.data;
      this.filterProjectsByKeyword = this.allCompanyProjects;

      return null;
    });
    this.userService.getUserDetails(+this.loggedUserId).subscribe((data) => {
      this.loggedUserCurrentHours = data.data.projectsMember.reduce((acc, val) => {
        if (val.endDate === null) {
          acc += val.hours;
        }
        return acc;
      }, 0);
    });
  }
}
