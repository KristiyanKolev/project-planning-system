import { ProjectsService } from './../../../core/services/projects.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { SkillService } from './../../../core/services/skill.service';
import { UserService } from 'src/app/core/services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder, FormControl } from '@angular/forms';
import { UserDTO } from 'src/app/models/users/user.dto';
import { SkillDTO } from 'src/app/models/skills/skill.dto';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css'],
})
export class CreateProjectComponent implements OnInit {
  public loggedUserId: string;
  public loggedManager: UserDTO;
  public employees: UserDTO[] = [];
  public skills: SkillDTO[];
  public createProjectForm: FormGroup;
  public employeeHours: Array<UserHours> = [];
  public membersFreeHours: object = {};
  public paramId = '';

  constructor(
    private readonly userService: UserService,
    private readonly skillService: SkillService,
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly projectService: ProjectsService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService
  ) {}

  calculateUserHours(id: number): void {
    const selectedUser = this.employeeHours.find((el) => +el.id === +id);
    if (!selectedUser) {
      return;
    }

    if (+selectedUser.hours === 0) {
      this.membersFreeHours[id] = 8;
    } else {
      this.membersFreeHours[id] = 8 - selectedUser.hours;
    }
  }

  getFreeHours(user: { controls: { id: FormControl } }) {
    const userId = (user.controls.id as FormControl).value;
    if (userId) {
      return this.membersFreeHours[userId];
    }
    return null;
  }

  createActivity(): FormGroup {
    return this.fb.group({
      id: [null],
      hours: ['', Validators.required],
      skill: this.fb.group({
        id: ['', Validators.required],
      }),
      members: this.fb.array([], Validators.required),
    });
  }

  clearMembers(index: number): void {
    const activities = this.createProjectForm.get('activities') as FormArray;
    const activity = activities.at(index);
    const members = activity.get('members') as FormArray;
    while (members.length !== 0) {
      members.removeAt(0);
    }
  }

  createMember(): FormGroup {
    return this.fb.group({
      id: [null],
      hours: ['', Validators.required],
      user: this.fb.group({
        id: ['', Validators.required],
      }),
    });
  }

  addActivity(): void {
    const activities = this.createProjectForm.get('activities') as FormArray;
    activities.push(this.createActivity());
  }

  removeActivity(index: number): void {
    const activities = this.createProjectForm.get('activities') as FormArray;
    activities.removeAt(index);
  }

  addMember(index: number): void {
    const activities = this.createProjectForm.get('activities') as FormArray;
    const activity = activities.at(index);
    const members = activity.get('members') as FormArray;
    members.push(this.createMember());
  }

  removeMember(activityIndex: number, memberIndex: number): void {
    const members = (this.activitiesArray.controls[activityIndex] as FormGroup).controls
      .members as FormArray;
    members.removeAt(memberIndex);
  }

  createProject(): void {
    const project = this.createProjectForm.value;
    const hasInvalidMember = project.activities.find((activity) =>
      activity.members.find((member) => {
        return this.membersFreeHours[member.user.id] < member.hours;
      })
    );
    if (hasInvalidMember) {
      alert('Invalid hours!');
    } else {
      this.projectService.createProject(project).subscribe(() => {
        this.router.navigate(['/projects']);
        this.notificationService.success('Project created successfully');
      });
    }
  }

  editProject(): void {
    const project = this.createProjectForm.value;
    const hasInvalidMember = project.activities.find((activity) =>
      activity.members.find((member) => {
        return this.membersFreeHours[member.user.id] < member.hours;
      })
    );
    if (hasInvalidMember) {
      this.notificationService.error('Invalid hours');
    } else {
      this.projectService.editProject(this.paramId, project).subscribe(() => {
        this.router.navigate(['/projects']);
        this.notificationService.success('Project saved successfully');
      });
    }
  }

  get activitiesArray(): FormArray {
    return this.createProjectForm.get('activities') as FormArray;
  }

  membersArray(index: number): FormGroup[] {
    return ((this.activitiesArray.controls[index] as FormGroup).controls.members as FormArray)
      .controls as FormGroup[];
  }

  addManager(manager: UserDTO): void {
    this.employees.push(manager);
  }

  filterEmployeesBySkill(activityIndex: number): UserDTO[] {
    const skillId = (((this.activitiesArray.controls[activityIndex] as FormGroup).controls
      .skill as FormGroup).controls.id as FormControl).value;

    return this.employees.filter((employee) => {
      return employee.skills.find((employeeSkill) => {
        const filtered = employeeSkill.id === +skillId;
        return filtered;
      });
    });
  }

  allUsersDailyInput(): void {
    this.employeeHours = [];

    this.employees.forEach((employee) => {
      const hours = employee.projectsMember.reduce((acc, val) => {
        if (val.endDate === null) {
          acc += val.hours;
        }
        return acc;
      }, 0);
      this.employeeHours.push({ id: employee.id, hours });
    });
  }

  checkTooManyHours(member: {
    controls: { user: { controls: { id: FormControl } }; hours: FormControl };
  }) {
    const freeHours = this.getFreeHours(member.controls.user);
    const newHours = (member.controls.hours as FormControl).value;
    return freeHours !== null && newHours && freeHours - newHours < 0;
  }

  ngOnInit(): void {
    this.createProjectForm = this.fb.group({
      id: [null],
      name: ['', Validators.required],
      description: ['', Validators.required],
      targetInDays: [null, Validators.required],
      activities: this.fb.array([], Validators.required),
    });

    this.authService.loggedUserData$.subscribe((userData) => {
      this.loggedUserId = userData.id;
      return;
    });

    this.userService.getUserDetails(+this.loggedUserId).subscribe((user) => {
      this.loggedManager = user.data;
      return;
    });

    this.userService.getAllBasicUsers().subscribe((data) => {
      this.employees = data.data;
      this.addManager(this.loggedManager);
      this.allUsersDailyInput();
      return;
    });

    this.skillService.getAllSkills().subscribe((data) => {
      this.skills = data.data;
      return;
    });

    this.route.paramMap.subscribe((params) => {
      this.paramId = params.get('id');
      console.log(this.paramId);
    });

    if (this.paramId) {
      this.projectService.projectDetails(this.paramId).subscribe((data) => {
        (this.createProjectForm.controls.name as FormControl).setValue(data.data.name);
        (this.createProjectForm.controls.description as FormControl).setValue(
          data.data.description
        );
        (this.createProjectForm.controls.targetInDays as FormControl).setValue(
          data.data.targetInDays
        );
        (this.createProjectForm.controls.id as FormControl).setValue(this.paramId);

        const activities = this.createProjectForm.get('activities') as FormArray;
        data.data.activities.forEach((activity) => {
          const newActivity = this.createActivity();
          (newActivity.controls.id as FormControl).setValue(activity.id);

          (newActivity.controls.hours as FormControl).setValue(activity.hours);
          ((newActivity.controls.skill as FormGroup).controls.id as FormControl).setValue(
            activity.skill.id
          );

          activities.push(newActivity);
          const members = newActivity.get('members') as FormArray;

          activity.members.forEach((member) => {
            if (member.endDate) {
              return;
            }
            const newMember = this.createMember();
            (newMember.controls.id as FormControl).setValue(member.id);
            (newMember.controls.hours as FormControl).setValue(member.hours);
            ((newMember.controls.user as FormGroup).controls.id as FormControl).setValue(
              member.user.id
            );
            members.push(newMember);
          });
        });
      });
    }
  }
}

interface UserHours {
  id: string;
  hours: number;
}
