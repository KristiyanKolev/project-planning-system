import { ProjectDTO } from 'src/app/models/projects/project.dto';
import { ProjectsService } from './../../../core/services/projects.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDTO } from 'src/app/models/users/user.dto';
import * as moment from 'moment';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
})
export class ProjectDetailsComponent implements OnInit {
  public currentProject: ProjectDTO;
  public reportingManager: UserDTO;
  public hoursLeft: number[];
  public daysLeft: number[];
  public estimatedTarget: number;
  public withinTarget: string;
  public projectId: string;
  public loggedUserId: string;

  constructor(
    private readonly projectService: ProjectsService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly auth: AuthService
  ) {}

  skillHoursLeft(): void {
    this.currentProject.activities.forEach((activity) => {
      const activityTotal = activity.hours;

      const hoursCompleted = activity.members.reduce((acc, member) => {
        if (member.endDate === null) {
          const startDate = member.startDate;
          const startDateConvert = moment(startDate, 'YYYY-MM-DD');

          const today = moment();
          const currentDate = moment(today, 'YYYY-MM-DD');

          const difference = currentDate.diff(startDateConvert, 'days');
          acc += member.hours * difference;

          return acc;
        } else {
          const startDate = member.startDate;
          const startDateConvert = moment(startDate, 'YYYY-MM-DD');

          const currentDate = moment(member.endDate, 'YYYY-MM-DD');

          const difference = currentDate.diff(startDateConvert, 'days');
          acc += member.hours * difference;

          return acc;
        }
      }, 0);

      const hoursLeft = activityTotal - hoursCompleted;
      this.hoursLeft.push(hoursLeft);

      return null;
    });
  }

  skillCompletionInDays(): void {
    this.currentProject.activities.forEach((activity, index) => {
      const dailyInput = activity.members.reduce((acc, member) => {
        if (member.endDate === null) {
          acc += member.hours;
        }
        return acc;
      }, 0);
      const divided = dailyInput === 0 ? 0 : Math.floor(this.hoursLeft[index] / dailyInput);
      this.daysLeft.push(divided);
    });
    this.estimatedTarget = this.daysLeft.reduce((acc, el) => {
      if (acc < el) {
        acc = el;
      }
      return acc;
    }, 0);
    const startDate = this.currentProject.startDate;
    const startDateConvert = moment(startDate, 'YYYY-MM-DD');

    const today = moment();
    const currentDate = moment(today, 'YYYY-MM-DD');

    const difference = currentDate.diff(startDateConvert, 'days');
    if (this.currentProject.targetInDays < difference + this.estimatedTarget) {
      this.withinTarget = 'No';
    } else {
      this.withinTarget = 'Yes';
    }
  }

  stopProject(id: string): void {
    this.projectService.stopProject(id).subscribe(() => {
      this.router.navigate(['/projects']);
    });
  }

  markAsCompleted(id: string): void {
    this.projectService.markProjectAsComplete(id).subscribe(() => {
      this.router.navigate(['/projects']);
    });
  }

  ngOnInit(): void {
    this.hoursLeft = [];
    this.daysLeft = [];

    this.auth.loggedUserData$.subscribe((data) => {
      this.loggedUserId = data.id;
      return;
    });

    this.route.params.subscribe((params) => {
      this.projectService.projectDetails(params.id).subscribe((data) => {
        this.projectId = data.data.id.toString();
        this.currentProject = data.data;
        this.skillHoursLeft();
        this.skillCompletionInDays();
        const manager = data.data.activities.filter((activity) => {
          return activity.members.find((member) => {
            return member.user.role === 'MANAGER';
          });
        });
        this.reportingManager = manager[0].members[0].user;
      });
      return;
    });
  }
}
