import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './components/projects/projects.component';
import { AllProjectsComponent } from './components/all-projects/all-projects.component';
import { EditProjectComponent } from './components/edit-project/edit-project.component';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';

@NgModule({
  declarations: [
    ProjectsComponent,
    AllProjectsComponent,
    EditProjectComponent,
    CreateProjectComponent,
    ProjectDetailsComponent,
  ],
  imports: [CommonModule, RouterModule, FormsModule, CommonModule, ReactiveFormsModule],
  exports: [],
})
export class ProjectsModule {}
