import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SkillDTO } from '../../../models/skills/skill.dto';
import { CreateSkillDTO } from '../../../models/skills/create-skill.dto';
import { ValidationService } from '../../../core/services/validation.service';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.css'],
})
export class AddSkillComponent {
  @Input() public skillsList: SkillDTO[];
  @Output() public readonly createSkillEmitter: EventEmitter<
    CreateSkillDTO
  > = new EventEmitter();

  public constructor(private readonly validationService: ValidationService) {}

  public addNewSkill(skillName: string): void {
    if (!this.validationService.validateNewSkill(skillName, this.skillsList)) {
      return;
    }

    const newSkill: CreateSkillDTO = { skill: skillName };
    this.createSkillEmitter.emit(newSkill);
  }
}
