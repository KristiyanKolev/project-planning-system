import { Component, OnInit, Input } from '@angular/core';
import { SkillDTO } from '../../../models/skills/skill.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-skills',
  templateUrl: './all-skills.component.html',
  styleUrls: ['./all-skills.component.css'],
})
export class AllSkillsComponent implements OnInit {
  @Input() public skillsList: SkillDTO[];

  public constructor(private readonly router: Router) {}

  public filterBySkill(skillName: string): void {
    this.router.navigate(['employees'], {
      queryParams: { skill: skillName },
    });
  }

  public ngOnInit(): void {}
}
