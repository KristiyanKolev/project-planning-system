import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { SharedModule } from '../../../shared/shared.module';
import { SkillService } from '../../../core/services/skill.service';
import { NotificationService } from '../../../core/services/notification.service';
import { SkillsPageComponent } from './skills-page.component';
import { AllSkillsComponent } from '../all-skills/all-skills.component';
import { AddSkillComponent } from '../add-skill/add-skill.component';

describe('SkillsPageComponent', () => {
  let notificationService: any;
  let skillService: any;

  let fixture: ComponentFixture<SkillsPageComponent>;
  let component: SkillsPageComponent;

  beforeEach(async(() => {
    jest.clearAllMocks();

    notificationService = {
      success: jest.fn(),
    };

    skillService = {
      getAllSkills: jest.fn(),
      createSkill: jest.fn(),
    };

    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [SkillsPageComponent, AllSkillsComponent, AddSkillComponent],
      providers: [SkillService, NotificationService],
    })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .overrideProvider(SkillService, { useValue: skillService })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(SkillsPageComponent);
        component = fixture.componentInstance;
      });
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });
});
