import { Component, OnInit } from '@angular/core';
import { SkillService } from '../../../core/services/skill.service';
import { SkillDTO } from '../../../models/skills/skill.dto';
import { CreateSkillDTO } from '../../../models/skills/create-skill.dto';
import { NotificationService } from '../../../core/services/notification.service';

@Component({
  selector: 'app-skills-page',
  templateUrl: './skills-page.component.html',
  styleUrls: ['./skills-page.component.css'],
})
export class SkillsPageComponent implements OnInit {
  public allSkills: SkillDTO[];

  public constructor(
    private readonly skillService: SkillService,
    private readonly notificationService: NotificationService
  ) {}

  public createSkill(skill: CreateSkillDTO): void {
    this.skillService.createSkill(skill).subscribe((response) => {
      this.allSkills = [...this.allSkills, response.data];
      this.notificationService.success('Skill created successfully');
    });
  }

  public ngOnInit(): void {
    this.skillService
      .getAllSkills()
      .subscribe((allSkills) => (this.allSkills = allSkills.data));
  }
}
