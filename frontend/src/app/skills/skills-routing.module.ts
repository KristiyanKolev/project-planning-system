import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../core/guards/admin.guard';
import { NgModule } from '@angular/core';

import { SkillsPageComponent } from './components/skills-page/skills-page.component';

const routes: Routes = [
  {
    path: '',
    component: SkillsPageComponent,
    pathMatch: 'full',
    canActivate: [AdminGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SkillsRoutingModule {}
