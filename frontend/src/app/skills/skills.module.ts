import { SkillsRoutingModule } from './skills-routing.module';
import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';

import { SkillsPageComponent } from './components/skills-page/skills-page.component';
import { AllSkillsComponent } from './components/all-skills/all-skills.component';
import { AddSkillComponent } from './components/add-skill/add-skill.component';

@NgModule({
  declarations: [SkillsPageComponent, AllSkillsComponent, AddSkillComponent],
  imports: [SkillsRoutingModule, SharedModule],
})
export class SkillsModule {}
